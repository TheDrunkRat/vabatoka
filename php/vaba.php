<script src="js/profile-page.js"></script>
<audio class="profile-audio" src="audio/vevi.mp3" type="audio/mp3" id="vevi"></audio>
<div class="container-fluid profile-page-container">
    <div class="row">
        <div class="col-sm-4 profile-sidebar" id="left">
            <div>
                <a class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                    <img class="img-fluid profile-photo rounded-circle" src="img/vevi_thumb.jpg" alt="Vevi" id="left">
                </a>
                <br>
            </div>
            <div>
                <h3><i class="fas fa-user"></i> Osobni podaci</h3>
                <p class="profile-value">27. 2. 1998.</p>
                <p class="profile-value-label">Datum rođenja</p>

                <a href="https://www.virovitica.hr/"><p class="profile-value">Virovitica</p></a>
                <p class="profile-value-label">Mjesto rođenja</p>

                <a href="https://www.osijek.hr/"><p class="profile-value">Osijek</p></a>
                <p class="profile-value-label">Mjesto prebivališta</p>

                <p class="profile-value">Muško</p>
                <p class="profile-value-label">Spol</p>
            </div>
            <div>
                <h3><i class="fas fa-volleyball-ball"></i> Hobiji</h3>
                <p class="profile-value">Bodybuilding</p>
                <p class="profile-value-label">Sportovi</p>

                <p class="profile-value">Fotografiranje i dizajn</p>
                <p class="profile-value-label">Ostalo</p>
            </div>
            <div>
                <h3><i class="fas fa-globe-europe"></i> Jezici</h3>
                <p class="profile-value">Engleski</p>
                <p class="profile-value-label">C1</p>
            </div>
            <div>
                <h3><i class="fas fa-users"></i> Društvene mreže</h3>
                <div>
                    <a href="https://www.facebook.com/valerian.bahnik"><button type="button" class="social-media-button btn btn-outline-primary" id="left"><i class="fab fa-facebook"></i></button></a>
                    <a href="https://www.instagram.com/valerian_bahnik/"><button type="button" class="social-media-button btn btn-outline-primary" id="left"><i class="fab fa-instagram"></i></button></a>
                    <a href="https://twitter.com/ValerianBahnik"><button type="button" class="social-media-button btn btn-outline-primary" id="left"><i class="fab fa-twitter"></i></button></a>
                    <a href="https://www.youtube.com/channel/UC2HO_zSsLWS4mBcZ1oddOCA"><button type="button" class="social-media-button btn btn-outline-primary" id="left"><i class="fab fa-youtube"></i></button></a>
                    <a href="https://www.linkedin.com/in/valerian-bahnik-708a85183/"><button type="button" class="social-media-button btn btn-outline-primary" id="left"><i class="fab fab fa-linkedin-in"></i></button></a>
                </div>
            </div>
        </div>
        <div class="col-sm-8 profile-content-container">
            <div class="row">
                <div class="col-sm-1 profile-audio-container d-flex justify-content-center align-self-center">
                    <button type="button" class="btn btn-outline-primary profile-audio-button play" id="left"><i class="fas fa-play" aria-hidden="true"></i></button>
                </div>
                <div class="col-sm">
                    <h1 class="profile-name" id="left">Valerian Bahnik</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h2><i class="fas fa-graduation-cap"></i> Formalno obrazovanje</h2>
                    <p class="profile-value">Prvostupnik inženjer elektrotehnike</p>
                    <p class="profile-value-label">2016 - danas | <a href="https://www.ferit.unios.hr/">FERIT Osijek</a></p>
                    <p class="profile-value">Tehničar za računarstvo</p>
                    <p class="profile-value-label">2012 - 2016 | <a href="https://www.tsd.hr/site/">Tehnička škola Daruvar</a></p>

                    <h2><i class="fas fa-briefcase"></i> Radno iskustvo</h2>
                    <p class="profile-value">Praktikant</p>
                    <p class="profile-value-label">2015 - 2016 | <a href="https://sab.hr/">SAB d.o.o.</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h2><i class="fas fa-smile"></i> Više o meni</h2>
                    <p class="profile-description">
                        Kroz svoju karijeru prvostupnika informatike (inženjera) naučio sam osnove rada u grupama,
                        komunikaciji, poduzetništvu, marketingu i sl.
                        U srednjoj školi upoznat sam s osnovama programiranja u #C/C++, PLC, Arduino itd.,
                        te radom s elektroničkim komponentama. Kao što je rad u Eagleu i izrada tiskane pločice te
                        slaganje komponenti na nju.
                    </p>
                    <p class="profile-description">
                        Svoje studiranje sam proveo dosta aktivno radeći na raznim projektima i seminarima s kolegama.
                        Većina tih seminara i projekata vezani uz samo upoznavanje programskih jezika te programa
                        u kojima smo učili raditi. Neki od programskih jezika koje sam upoznao tijekom studiranja su
                        Python, rad s bazama podataka i njenom manipulacijom SQL, objektno-orijentirani Java,
                        osnove web jezika HTML5, CSS te JS i PHP.
                    </p>
                    <p class="profile-description">
                        Kroz kolegije Web programiranje i Dizajn korisničkog sučelja imao sam priliku upoznati se sa
                        "drugom stranom" frontendom. Tako su započeli moji hobiji. Rad u Adobe programima kao što su
                        Photoshop, Illustrator, Lightroom, Premiere Pro, XD.
                    </p>
                    <p class="profile-description">
                        Trenutno završavam studij te radim na izradi Android aplikacije za jednu pivovaru kako bi
                        olakšao njihovo poslovanje i prelazak u digitalni svijet. Jedan od radova koji je ugledao
                        svijetlo dana je portabilna vremenska stanica.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <a class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                        <img src="img/vevi1_thumb.gif" class="img-thumbnail profile-thumbnail" alt="Slika 1" id="left">
                    </a>
                </div>
                <div class="col-sm-4">
                    <a onmouseup='setEnlargedImage("img/vevi2.bmp");' class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                        <img src="img/vevi2_thumb.bmp" class="img-thumbnail profile-thumbnail" alt="Slika 2" id="left">
                    </a>
                </div>
                <div class="col-sm-4">
                    <a class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                        <img src="img/vevi3_thumb.webp" class="img-thumbnail profile-thumbnail" alt="Slika 3" id="left">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm profile-video-container align-self-center justify-content-center">
                    <iframe id="left" src="https://www.youtube-nocookie.com/embed/UyQnq8esheM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
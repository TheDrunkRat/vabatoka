<script src="js/profile-page.js"></script>
<audio class="profile-audio" src="audio/tomo.mp3" type="audio/mp3" id="tomo"></audio>
<div class="container-fluid profile-page-container" id="right">
    <div class="row">
        <div class="col-sm-4 profile-sidebar" id="right">
            <div>
                <a class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                    <img class="img-fluid profile-photo rounded-circle" src="img/tomo_thumb.jpg" alt="Tomo" id="right">
                </a>
                <br>
            </div>
            <div>
                <h3><i class="fas fa-user"></i> Osobni podaci</h3>
                <p class="profile-value">20. 9. 1996.</p>
                <p class="profile-value-label">Datum rođenja</p>

                <p class="profile-value"><a href="https://www.slavonski-brod.hr/">Slavonski Brod</a></p>
                <p class="profile-value-label">Mjesto rođenja</p>

                <p class="profile-value"><a href="https://www.osijek.hr/">Osijek</a></p>
                <p class="profile-value-label">Mjesto prebivališta</p>

                <p class="profile-value">Muško</p>
                <p class="profile-value-label">Spol</p>
            </div>
            <div>
                <h3><i class="fas fa-volleyball-ball"></i> Hobiji</h3>
                <p class="profile-value">Šah, pikado i bilijar</p>
                <p class="profile-value-label">Sportovi</p>

                <p class="profile-value">Grafički dizajn</p>
                <p class="profile-value-label">Ostalo</p>
            </div>
            <div>
                <h3><i class="fas fa-globe-europe"></i> Jezici</h3>
                <p class="profile-value">Engleski</p>
                <p class="profile-value-label">C2</p>

                <p class="profile-value">Njemački</p>
                <p class="profile-value-label">A1</p>

                <p class="profile-value">Ruski</p>
                <p class="profile-value-label">A1</p>
            </div>
            <div>
                <h3><i class="fas fa-users"></i> Društvene mreže</h3>
                <div>
                    <a href="https://www.facebook.com/TomislavKaucic"><button type="button" class="social-media-button btn btn-outline-primary" id="right"><i class="fab fa-facebook"></i></button></a>
                    <a href="https://www.instagram.com/tkaucic/"><button type="button" class="social-media-button btn btn-outline-primary" id="right"><i class="fab fa-instagram"></i></button></a>
                    <a href="https://twitter.com/TheDrunkRat"><button type="button" class="social-media-button btn btn-outline-primary" id="right"><i class="fab fa-twitter"></i></button></a>
                    <a href="https://www.youtube.com/channel/UCTKQRmuYBmJkDagnREnadAQ"><button type="button" class="social-media-button btn btn-outline-primary" id="right"><i class="fab fa-youtube"></i></button></a>
                    <a href="https://www.linkedin.com/in/tomislavkaucic/"><button type="button" class="social-media-button btn btn-outline-primary" id="right"><i class="fab fa-linkedin-in"></i></button></a>
                </div>
            </div>
        </div>
        <div class="col-sm-8 profile-content-container">
            <div class="row">
                <div class="col-sm-1 profile-audio-container d-flex justify-content-center align-self-center">
                    <button type="button" class="btn btn-outline-primary profile-audio-button play" id="right"><i class="fas fa-play" aria-hidden="true"></i></button>
                </div>
                <div class="col-sm">
                    <h1 class="profile-name" id="right">Tomislav Kaučić</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h2><i class="fas fa-graduation-cap"></i> Formalno obrazovanje</h2>
                    <p class="profile-value">Prvostupnik inženjer elektrotehnike</p>
                    <p class="profile-value-label">2016 - danas | <a href="https://www.ferit.unios.hr/">FERIT Osijek</a></p>
                    <p class="profile-value">Tehničar za računalstvo</p>
                    <p class="profile-value-label">2011 - 2015 | <a href="http://tssb.hr/">Tehnička škola Slavonski Brod</a></p>

                    <h2><i class="fas fa-briefcase"></i> Radno iskustvo</h2>
                    <p class="profile-value">Asistent koordinatora projekta</p>
                    <p class="profile-value-label">2016 - 2018 | <a href="http://europski-dom-sb.hr/">Europski dom Slavonski Brod</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h2><i class="fas fa-smile"></i> Više o meni</h2>
                    <p class="profile-description">
                        Kroz Erasmus+ i EYF programe sudjelovao sam na seminarima i razmjenama mladih, te sam više
                        puta radio u timu s osobama iz različitih kultura na zajedničkom cilju, najčešće radeći na temi
                        društvenog poduzetništva, nevladinih organizacija, analiza poduzeća, izrade strategije poduzeća,
                        rješavanja problema u poduzeću i sl.
                    </p>
                    <p class="profile-description">
                        Kroz nekoliko projekata u sklopu Erasmus+ programa Europske unije naučio sam osnovne pojmove
                        o društvenom poduzetništvu, analizi poslovne pozicije, marketingu, promoviranju proizvoda
                        različitim
                        dobnim skupinama i sl.
                    </p>
                    <p class="profile-description">
                        Trenutno vodim tim i radim aktivno na prototipu uređaja koji prezentiramo na natjecanjima. Kao
                        član
                        nevladine organizacije koordinirao sam domaće i strane volontere na raznim lokalnim aktivnostima
                        vezanim uz Europsku uniju.
                    </p>
                    <p class="profile-description">
                        Prvi pravi hobi moga života bilo je programiranje te sam ga kroz formalno obrazovanje pretvorio
                        u zanimanje. Izvan formalnog obrazovanja koristio sam C#, Javu i Python te sam upoznat s
                        osnovama Dart programskog jezika.
                    </p>
                    <p class="profile-description">
                        Ipak, s tim sam dobio problem: trebao mi je novi hobi koji će me otjerati od starog hobija.
                        Upravo zato sam se počeo baviti grafičkim dizajnom. Naučio sam koristiti Adobe alate (Photoshop,
                        Illustrator, InDesign, Premiere Pro i XD) i osmislio sam nekoliko plakata koji su ugledali
                        svjetlo dana. Tu i tamo se prijavim na koji natječaj pa mi se zna dogoditi da rješenje završi u
                        galeriji :)
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <a class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                        <img src="img/tomo1_thumb.gif" class="img-thumbnail profile-thumbnail" alt="Slika 1" id="right">
                    </a>
                </div>
                <div class="col-sm-4">
                    <a onmouseup='setEnlargedImage("img/tomo2.bmp");' class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                        <img src="img/tomo2_thumb.bmp" class="img-thumbnail profile-thumbnail" alt="Slika 2" id="right">
                    </a>
                </div>
                <div class="col-sm-4">
                    <a class="thumbnail-link" data-toggle="modal" data-target="#imageModal">
                        <img src="img/tomo3_thumb.webp" class="img-thumbnail profile-thumbnail" alt="Slika 3" id="right">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm profile-video-container align-self-center justify-content-center">
                    <iframe id="right" src="https://www.youtube-nocookie.com/embed/ZNIE0sVBuP8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
$(document).ready(function () {


    function setEnlargedImage(imageSource) {
        $('img.img-enlarged').attr('src', imageSource);
    };

    $('a.thumbnail-link').mouseup(function () {
        var src = $(this).children('img').attr('src');
        src = src.replace('_thumb', '');
        setEnlargedImage(src);
    });

    $('button.profile-audio-button').mouseup(function () {
        if ($(this).hasClass('play') == true) {
            $(this).html('<i class="fas fa-pause"></i>');
            $(this).removeClass('play');
            $(this).addClass('pause');
            $('.profile-audio')[0].play();
        }
        else {
            $(this).removeClass('pause');
            $(this).addClass('play');
            $(this).html('<i class="fas fa-play"></i>');
            $('.profile-audio')[0].pause();
            $('.profile-audio')[0].currentTime = 0;
        }

    });

    $('.profile-audio').on('ended', function () {
        $('button.profile-audio-button').removeClass('pause');
        $('button.profile-audio-button').addClass('play');
        $('button.profile-audio-button').html('<i class="fas fa-play"></i>');
        $('.profile-audio')[0].pause();
        $('.profile-audio')[0].currentTime = 0;
    });
});
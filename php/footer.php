<div class="row footer">
    <div class="col-sm-3">
        <div class="footer-logo-container">
            <a href="."><img src="img/logo.png" class="img-fluid footer-logo" alt="VaBaToKa"></a>
        </div>
    </div>
    <div class="col-sm-9 footer-text-container">
        <div class="row">
            <div class="col-sm">
                <p class="footer-disclaimer">
                    Web stranica je izrađena za vrijeme pohađanja <a href="https://www.ferit.unios.hr/">Fakulteta
                        elektrotehnike,
                        računarstva i informacijskih tehnologija u Osijeku</a> u sklopu kolegija <a href="https://loomen.carnet.hr/course/view.php?id=3626">Multimedijska tehnika</a>.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <ul>
                    <li>Naše stranice:</li>
                    <li><a href="#vaba">Valerian Bahnik</a></li>
                    <li><a href="#toka">Tomislav Kaučić</a></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <ul>
                    <li>Naše kolege:</li>
                    <li><a href="http://osobnastranica1.000webhostapp.com/pocetna-stranica">Filip Šangut &amp; Ružica Šapina</a></li>
                    <li><a href="http://mmtcvml.atwebpages.com/">Matej Landeka &amp; Mihael Lneniček</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

$(document).ready(function () {
    var animationSpeed = 200;

    $('div.footer-content-container').load('php/footer.php');

    function goToPage(page) {
        $('div.content').fadeOut(animationSpeed, function () {
            $('div.content').hide();
            $('div.content').empty();
            $('div.content').load(page);
            $('div.content').fadeIn(animationSpeed);
        });
    };

    window.onhashchange = function () {
        location.reload();
    }

    if (window.location.hash == '#toka') {
        goToPage('php/toka.php');
        $('body').css('background-color', 'var(--right-color)');
        $('button.btn-chevron#right').hide();
        $('div.nav-sidebar').css('background-color', 'var(--right-color)');
        $("button.btn-chevron").hover(function () {
            $(this).css("color", "var(--left-color)")
        });
        $("button.btn-chevron").mouseleave(function () {
            $(this).css("color", "var(--white)")
        });
        $("div.nav-topbar").css("background-color", "var(--right-color)");
    }
    else if (window.location.hash == '#vaba') {
        goToPage('php/vaba.php');
        $('body').css('background-color', 'var(--left-color)');
        $('button.btn-chevron#left').hide();
        $('div.nav-sidebar').css('background-color', 'var(--left-color)');
        $("div.nav-topbar").css("background-color", "var(--left-color)");
        $("button.btn-chevron").hover(function () {
            $(this).css("color", "var(--right-color)")
        });
        $("button.btn-chevron").mouseleave(function () {
            $(this).css("color", "var(--white)")
        });
    } else {
        goToPage('php/menu.php');
        $('button.btn-chevron').show();
    }
});
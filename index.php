<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:image" content="/img/logo.png">
  <title>VaBaToKa</title>

  <link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap-grid.css">
  <link rel="stylesheet" href="css/bootstrap-reboot.css">
  <script src="js/jquery-3.4.0.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/main.js"></script>
  <script src="js/jquery.jplayer.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
  <link rel="manifest" href="site.webmanifest">
  <link rel="mask-icon" href="safari-pinned-tab.svg" color="#333333">
  <meta name="msapplication-TileColor" content="#333333">
  <meta name="theme-color" content="#eeeeee">
</head>

<body>
  <div class="row wrapper">
    <div class="container-fluid">
      <div class="row page-container">
        <div class="col-sm-1 nav-sidebar text-center" id="left">
          <a href="#vaba"><button type="button" class="btn btn-primary-outline btn-chevron" id="left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button></a>
        </div>
        <div class="col-sm content-container">
          <div class="row nav-topbar">
            <div class="pull-left">
              <a href="#vaba"><button type="button" class="btn btn-primary-outline btn-chevron" id="left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button></a>
            </div>
            <div class="pull-right">
              <a href="#toka"><button type="button" class="btn btn-primary-outline btn-chevron" id="right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button></a>
            </div>
          </div>
          <div class="row content h-100">

          </div>
        </div>
        <div class="col-sm-1 nav-sidebar text-center" id="right">
          <a href="#toka"><button type="button" class="btn btn-primary-outline btn-chevron" id="right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button></a>
        </div>
      </div>
      <div class="row footer-container">
        <div class="col-sm-1 nav-sidebar" id="left"></div>
        <div class="col-sm-10 footer-content-container">
        </div>
        <div class="col-sm-1 nav-sidebar" id="right"></div>
      </div>
    </div>
  </div>

  <!--Modal za prikaz slike-->
  <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-body justify-content-center align-self-center">
          <img src="" class="img-fluid img-enlarged" alt="Slika">
        </div>
      </div>
    </div>
  </div>
</body>

</html>
<script src="./js/menu.js"></script>
<div class="container-fluid justify-content-center align-self-center">
    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-3 d-flex justify-content-center">
            <a href="#vaba"><img src="img/vevi_thumb.jpg" class="img-fluid rounded-circle intro-image" id="left" alt="Valerian Bahnik"></a>
        </div>
        <div class="col-sm-4 intro-title-container">
            <h1 class="intro-title" id="introTitle">Choose your fighter!</h1>
        </div>
        <div class="col-sm-3 d-flex justify-content-center">
            <a href="#toka"><img src="img/tomo_thumb.jpg" class="img-fluid rounded-circle intro-image" id="right" alt="Tomislav Kaučić"></a>
        </div>
        <div class="col-sm-1">
        </div>
    </div>
</div>
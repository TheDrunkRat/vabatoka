$(document).ready(function () {
    $('.intro-image').mouseenter(function () {
        $('#introTitle').hide();
        $('#introTitle').fadeIn();
        $('#introTitle').text($(this).attr('alt'));
    });

    $('.intro-image').mouseleave(function () {
        $('#introTitle').hide();
        $('#introTitle').fadeIn();
        $('#introTitle').text('Choose your fighter!');
    });

    $('button.btn-chevron#left, img.intro-image#left').mouseup(function () {
        goToPage('php/vaba.php');
    });

    $('button.btn-chevron#right, img.intro-image#right').mouseup(function () {
        goToPage('php/toka.php');
    });
});